def reverser
  words_to_reverse = yield.split
  words_to_reverse.map(&:reverse).join(' ')
end

def adder(number = 1)
  yield + number
end

def repeater(number = 1)
  number.times { yield }
end
