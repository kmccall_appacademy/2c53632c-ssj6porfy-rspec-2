def measure(number = 1)
  total_time = 0
  number.times do
    beginning_time = Time.now
    yield
    ending_time = Time.now
    total_time += ending_time - beginning_time
  end
  total_time / number
end
